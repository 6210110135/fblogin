const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const jwt = require('jsonwebtoken')
const axios = require('axios')
const app = express()
const port = 8080
const TOKEN_SECRET = 'b3fe26ddcc8d7e21532f8ea909b1184db69dd14712f21ad83260c0f62cf536e3265d11187576501ab81b8e8d3b922fdb0197b7ea9bde0ba337f0f99669b752fa'

const authenticated = async (req, res, next) => {
    const auth_header = req.headers['authorization']
    const token = auth_header && auth_header.split(' ')[1]
    if (!token)
        return res.sendStatus(401)

    jwt.verify(token, TOKEN_SECRET, (err, info) => {
        if (err) return res.sendStatus(403)
        req.username = info.username
        next()
    })
}

app.use(cors())

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/api/info', authenticated, (req, res) => {
    res.send({ ok: 1, username: req.username })
})

app.post('/api/login', bodyParser.json(), async (req, res) => {
    let token = req.body.token
    let result = await axios.get('https://graph.facebook.com/me',{ 
        params: {
            fields: "id,name,email",
            access_token: token
        }
    })
    console.log(result.data)
    if(!result.data.id){
        res.sendStatus(403) //login ไม่สำเร็จ
        return
    }
    let data = {
            username: result.data.email
        }
        let access_token = jwt.sign(data, TOKEN_SECRET,{expiresIn: '1800s'})
        res.send({access_token,username: data.username})
})

app.listen(port, () => {
  console.log(`Server listening on port ${port}`)
})